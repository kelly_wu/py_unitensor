from distutils.core import setup, Extension
from Cython.Build import cythonize
import numpy

ext = Extension("pyUni10", sources=["pyUni10.pyx", ],
				include_dirs=[numpy.get_include()],
				language="c++")

setup(name="pyUni10", ext_modules=cythonize(ext))