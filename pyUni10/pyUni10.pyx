import cython
import numpy as np
cimport numpy as np
from cython.operator import dereference as deref
import copy

################
## Qnum class ##
################
cdef class Qnum:
	cdef readonly int m_U1, m_prt, m_prtF
	cdef readonly bint Fermionic

	## @brief Constructor.

	## @param U1    Quantum numbers, default to 0.
	## @param prt    Particle parity, default to "PRT_EVEN"
	## @param prtF    Fermionic number parity, default to "PRTF_EVEN"
	def __init__(self, int U1=0, str prt="PRT_EVEN", str prtF="PRTF_EVEN"):
		try:
			self.Fermionic = False
			self.m_U1 = U1
			if prt is "PRT_EVEN":
				self.m_prt = 0
			elif prt is "PRT_ODD":
				self.m_prt = 1
			else:
				raise ValueError("Wrong value for prt.")

			if prtF is "PRTF_EVEN":
				self.m_prtF = 0
			elif prtF is "PRTF_ODD":
				self.Fermionic = True
				self.m_prtF = 1
			else:
				raise ValueError("Wrong value for prtF.")
		except:
			del self
			raise

	## @brief Turn Qnum object to string.

	## @Code:
	## (U1 = 1, P = 1, 0)
	## @Encode:
	## The quantum number is one, particle parity is "PRT_ODD" and fermionic number parity is "PRTF_EVEN".
	def __str__(self):
		return "(U1 = {0}, P = {1}, {2})".format(self.m_U1, self.m_prt, self.m_prtF)

	## @brief Print Qnum object.

	## Overload the () operator with print function.
	def __call__(self):
		print(self)

	## @brief Special method, which defines the hash value of a Qnum.

	## The hash value is given by the hash method of Qnum.
	def __hash__(self):
		return self.hash()

	## @brief Compare to Qnum's.

	## @param other    Qnum compared to self.
	## All kinds of comparison are available.
	def __richcmp__(self, Qnum other, cmp):
		if cmp == 0:
			return self.hash() < other.hash()
		elif cmp == 1:
			return self.hash() <= other.hash()
		elif cmp == 2:
			return (self.m_U1==other.m_U1) and (self.m_prt==other.m_prt) and (self.m_prtF==other.m_prtF)
		elif cmp == 3:
			return not (self == other)
		elif cmp == 4:
			return not (self <= other)
		elif cmp == 5:
			return not (self < other)

	## @brief Negate Qnum.

	## Turn the quantum numbers U1 to -U1.
	## @param q1    Qnum to negate.
	## @return A new Qnum object, which is the negation of q1.
	def __neg__(q1):
		if q1.m_prt == 0:
			prt = "PRT_EVEN"
		else:
			prt = "PRT_ODD"
		if q1.m_prtF == 0:
			prtF = "PRTF_EVEN"
		else:
			prtF = "PRT_ODD"
		q2 = Qnum(-q1.m_U1, prt, prtF)
		return q2

	## @brief Multiplication of two Qnum's.

	## @param q1, q2    Qnum's to multiply to each other.
	## @return A new Qnum object, which is the multiplication of q1 and q2.
	## New U1 = Addition of U1 of q1 and q2.
	## New prt = Bitwise XOR of prt of q1 and q2.
	## New prtF = Bitwise XOR of prtF of q1 and q2.
	def __mul__(q1, q2):
		if (q1.m_prt^q2.m_prt) == 0:
			prt = "PRT_EVEN"
		else:
			prt = "PRT_ODD"
		if (q1.m_prtF^q2.m_prtF) == 0:
			prtF = "PRTF_EVEN"
		else:
			prtF = "PRTF_ODD"
		q3 = Qnum((q1.m_U1+q2.m_U1), prt, prtF)
		return q3

	cpdef bint isFermionic(self):
		return self.Fermionic

	cpdef Qnum copy(self):
		new_q = Qnum()

		new_q.m_U1 = self.m_U1
		new_q.m_prt = self.m_prt
		new_q.m_prtF = self.m_prtF
		new_q.Fermionic = self.Fermionic

		return new_q

	## @brief Access quantum number of Qnum.

	## Returns the quantum number of the Qnum.
	## @return Quantun number, as integer.
	cpdef int U1(self):
		return self.m_U1

	## @brief Access parity number of Qnum/

	## Returns parity number of Qnum.
	## @return Parity number, as string.
	cpdef str prt(self):
		if self.m_prt == 0:
			return "PRT_EVEN"
		elif self.m_prt == 1:
			return "PRT_ODD"

	## @brief Access fermionic number parity of Qnum.

	## Returns fermionic number parity of Qnum.
	## @return Fermionic number parity, as string.
	cpdef str prtF(self):
		if self.m_prtF == 0:
			return "PRTF_EVEN"
		elif self.m_prtF == 1:
			return "PRTF_ODD"

	## @brief Assign content to Qnum.

	## Assign quantum numbers, particle parity and fermionic number parity to a Qnum,
	## replacing the current content.
	## @param U1    Quantum numbers, default to 0.
	## @param prt    Particle parity, default to "PRT_EVEN"
	## @param prtF    Fermionic number parity, default to "PRTF_EVEN"
	cpdef void assign(self, int U1=0, str prt="PRT_EVEN", str prtF="PRTF_EVEN"):
		tmp_U1, tmp_prt, tmp_prtF = self.m_U1, self.m_prt, self.m_prtF

		try:
			self.m_U1 = U1
			if prt is "PRT_EVEN":
				self.m_prt = 0
			elif prt is "PRT_ODD":
				self.m_prt = 1
			else:
				raise ValueError("Wrong value for prt.")

			if prtF is "PRTF_EVEN":
				self.m_prtF = 0
			elif prtF is "PRTF_ODD":
				self.Fermionic = True
				self.m_prtF = 1
			elif prtF is "":
				pass
			else:
				raise ValueError("Wrong value for prtF.")
		except:
			self.m_U1, self.m_prt, self.m_prtF = tmp_U1, tmp_prt, tmp_prtF
			raise

	## @brief Generate hash value of Qnum.

	## Generate a hash value of Qnum.
	## Hash values are different if Qnum's are inequal.
	## @return Hash value, as long integer.
	cpdef long hash(self):
		return self.m_U1*4 + self.m_prt*2 + self.m_prtF

################
## Bond class ##
################
cdef class Bond:
	cdef readonly int m_type, m_dim
	cdef readonly list Qnums, Qdegs, offsets
	cdef readonly int count

	## @brief Constructor

	## @param tp    Type of bond. Only take "BD_IN" and "BD_OUT".
	## @param qnums    A list of Qnum's.
	def __init__(self, str tp="BD_IN", list qnums=[Qnum()]):
		self.m_type = 0			## Type of Bond.
		self.m_dim = 0			## Dimension of bond.
		self.Qnums = []			## Quantum numbers.
		self.Qdegs = []			## Degeneracy in each quantum sector.
		self.offsets = []		## Offset of each quantum number.
		try:
			if tp is "BD_IN":
				self.m_type = 1
			elif tp is "BD_OUT":
				self.m_type = -1
			else:
				raise ValueError("Wrong value for bond type.")

			self.setting(qnums)
		except:
			del self
			raise

	## @brief Turn bond object to string.

	## @Code
	## IN : (U1 = 1, P = 0, 0)|1, (U1 = 0, P = 0, 0)|2, (U1 = -1, P = 0, 0)|1, Dim = 4
	## @Encode
	## The bond is an imcoming bond with three Qnum's : one U1=1, two U1=0, one U1=1.
	## The dimension of the bond is four.
	def __str__(self):
		s = ""
		if self.m_type == 1:
			s += "IN : "
		else:
			s += "OUT : "
		for i, Qnum in enumerate(self.Qnums):
			s = s + str(Qnum) + "|" + str(self.Qdegs[i]) + ", "
		s = s + "Dim = " + str(self.m_dim)
		return s

	## @brief Comparison of two bonds.

	## @param other    Bond compared to self.
	## Only "==" operator is available.
	def __richcmp__(self, Bond other, cmp):
		if cmp == 2:
			return (self.m_type==other.m_type) and (self.Qnums==other.Qnums) and (self.Qdegs==other.Qdegs)

	def __call__(self):
		print(self)

	## @brief Construct a Bond class.

	## Check if the input list of Qnum's is available.
	## Setup attributes of a Bond object.
	cdef void setting(self, list qnums):
		try:
			if len(qnums) == 0:
				raise ValueError("Cannot create a bond of dimension 0.")

			##for i in qnums:
			##	if type(i) is not Qnum:
			##		raise ValueError("All items in input list should be Qnum objects.")

			count = 0
			for i in range(len(qnums)):
				if i == 0 or (qnums[i] != qnums[i-1]):
					tmp_q = qnums[i].copy()
					self.Qnums.append(tmp_q)
					self.Qdegs.append(1)
					self.offsets.append(self.m_dim)
					count += 1
				else:
					self.Qdegs[count-1] += 1
				self.m_dim += 1
		except:
			raise

	## @brief Access bond type.

	## Returns the type of bond.
	## @return Type of bond, as a string.
	cpdef str type(self):
		if self.m_type == 1:
			return "BD_IN"
		elif self.m_type == -1:
			return "BD_OUT"

	## @brief Access bond dimension.

	## Returns the dimension of bond.
	## @return Dimension of bond, as an integer.
	cpdef int dim(self):
		return self.m_dim

	## @brief Access quantum numbers of bond.

	## Returns a list of quantum numbers of bond, the size of the returned list is the same as dimension.
	## @return List of Qnum.
	cpdef list Qlist(self):
		qlist = []
		for i, q in enumerate(self.Qnums):
			for j in range(self.Qdegs[i]):
				qlist.append(q.copy())
		return qlist

	## @brief Access degeneracy of bond.

	## Returns a dictionary of degeneracies for quantum numbers.
	## @return Dictionary of Qnum's to their degeneracies.
	cpdef dict degeneracy(self):
		hst = {}
		for i, q in enumerate(self.Qnums):
			if q not in hst.keys():
				hst[q.copy()] = self.Qdegs[i]
			else:
				hst[q.copy()] += self.Qdegs[i]
		return hst

	## @brief Assign bond content.

	## Assign type tp with list of Qnum's qnums to bond, replace the current content.
	## @param tp    Type of bond. Only take "BD_IN" and "BD_OUT".
	## @param qnums    A list of Qnum's.
	cpdef void assign(self, str tp, list qnums):
		(tmp_type, tmp_dim, tmp_Qnums, tmp_Qdegs, tmp_offsets) = (self.m_type, self.m_dim, self.Qnums, self.Qdegs, self.offsets)
		try:
			self.m_type = 0
			self.m_dim = 0
			self.Qnums = []
			self.Qdegs = []
			self.offsets = []
			if tp is "BD_IN":
				self.m_type = 1
			elif tp is "BD_OUT":
				self.m_type = -1
			else:
				raise ValueError("Wrong value for bond type.")

			self.setting(qnums)
		except:
			self.m_type = tmp_type
			self.m_dim = tmp_dim
			self.Qnums = tmp_Qnums
			self.Qdegs = tmp_Qdegs
			self.offsets = tmp_offsets
			raise

	## @brief Change type of bond.

	## Changes the bond type and Qnum's in the bond.
	## If the bond type is changed from outgoing to incoming or vice versa,
	## Qnum's are changed to -Qnum's.
	## @param tp    Type of bond. Only take "BD_IN" and "BD_OUT".
	cpdef Bond change(self, str tp):
		new_bd = self.copy()
		if self.type() != tp:
			if tp is "BD_IN":
				new_bd.m_type = 1
			elif tp is "BD_OUT":
				new_bd.m_type = -1
			else:
				raise ValueError("Wrong value for bond type.")
			for i, q in enumerate(new_bd.Qnums):
				new_bd.Qnums[i] = -q
		return new_bd

	## @brief Dummy change type of bond.

	## Only changes the bond type.
	## @param tp    Type of bond. Only take "BD_IN" and "BD_OUT".
	cpdef Bond dummy_change(self, str tp):
		new_bd = self.copy()
		if self.type() != tp:
			if tp is "BD_IN":
				new_bd.m_type = 1
			elif tp is "BD_OUT":
				new_bd.m_type = -1
			else:
				raise ValueError("Wrong value for bond type.")
		return new_bd

	## @brief Combine two bonds.

	## Combine bond with another bond bd, and expand the bond dimension by direct product
	## of Qnum's in the two bonds. The resulting bond type is unchanged.
	## The bond self will be replace by the combined bond.
	## @param bd    Bond to combine.
	cpdef Bond combine(self, Bond bd):
		try:
			BondA = self.copy()
			BondB = bd.copy()
			if BondA.m_type == 1:
				BondB.change("BD_IN")
			elif BondA.m_type == -1:
				BondB.change("BD_OUT")
			qnums = []
			qdegs = []
			BondA.offsets = []
			BondA.m_dim = 0
			count = 0
			for i, q in enumerate(BondA.Qnums):
				for d in range(BondA.Qdegs[i]):
					for j, qq in enumerate(BondB.Qnums):
						qnum = q * qq
						qdim = BondB.Qdegs[j]
						if (len(qnums) == 0) or not(qnum == qnums[count-1]):
							qnums.append(qnum)
							qdegs.append(qdim)
							BondA.offsets.append(BondA.m_dim)
							count += 1
						else:
							qdegs[count-1] += qdim
						BondA.m_dim += qdim
			BondA.Qnums = qnums
			BondA.Qdegs = qdegs

			return BondA
		except:
			raise

	cpdef Bond copy(self):
		tp = self.type()
		qnums = self.Qlist()
		new_bd = Bond(tp, qnums)
		return new_bd

#####################
## UniTensor class ##
#####################
cdef class UniTensor:
	cdef readonly str name ## Name of UniTensor.
	cdef readonly int status ## Check initialization. 1 = initialized, 2 = HAVEBOND, 3 = HAVEELEM, 4 = HAVEELEM and HAVEBOND
	cdef readonly list elem ## Array of elements.
	cdef readonly int RBondNum  ## Row bond number.
	cdef readonly int RQdim, CQdim ## Dimension of quantum numbers in row/column bonds.
	cdef readonly int m_elemNum ## Element numbers in UniTensor.
	cdef readonly int COUNTER, ELEMNUM, MAXELEMNUM, MAXELEMTEN
	#cdef readonly int HAVEBOND ## A flag for initialization.
	#cdef readonly int HAVEELEM ## A flag for having elements assigned.
	cdef readonly list bonds, labels
	cdef readonly dict blocks ## Map Qnum's to corresponding blocks.
	cdef readonly dict RQidx2Blk ## Row Qnum index to Block.
	cdef readonly dict QidxEnc
	cdef readonly dict RQidx2Off, CQidx2Off ## The row/column offset starts from the Block origin of Qnum.
	cdef readonly dict RQidx2Dim, CQidx2Dim ## The row/column dimension of Qnum's.
	cdef int i, j, k ## Integer iterators.
	cdef Qnum k1, k2 ## Qnum iterators.

	## @brief Constructor.

	## @param bds    A list of bonds.
	## @param _name    The name of UniTensor.
	def __init__(self, list bds=[], str _name=""):
		self.bonds = []
		for i in bds:
			new_bd = i.copy()
			self.bonds.append(new_bd)
		
		self.name = _name
		self.status = 1
		self.RBondNum = 0
		self.RQdim = 0
		self.CQdim = 0
		self.m_elemNum = 0

		self.labels = []
		self.blocks = {}

		self.RQidx2Blk = {}
		self.QidxEnc = {}
		self.RQidx2Dim = {}
		self.CQidx2Dim = {}
		self.RQidx2Off = {}
		self.CQidx2Off = {}

		self.ELEMNUM = 0
		self.COUNTER = 0
		self.MAXELEMNUM = 0
		self.MAXELEMTEN = 0
		#self.HAVEBOND = 1
		#self.HAVEELEM = 2
		
		self.elem = [0]
		self.initUniT()
		##self.elem = [0] * self.m_elemNum
		

	## @brief Initialize UniTensor.

	## Set status and labels of UniTensor.
	cdef void initUniT(self):
		try:
			if len(self.bonds) > 0: ## Have bonds.
				self.m_elemNum = self.grouping()
				if len(self.blocks) <= 0: ## No block in UniTensor, error.
					s = "There is no symmetry block with the given bonds:\n"
					for i in self.bonds:
						s = s + str(i) + "	"
					raise RuntimeError(s)

				self.labels = [0] * len(self.bonds)
				for i in range(len(self.bonds)):
					self.labels[i] = i
				self.status = 2

			else: ## No bonds.
				q0 = Qnum()
				self.blocks[q0] = np.zeros(shape=(1, 1))
				self.elem[0] = 0
				self.RBondNum = 0
				self.RQdim = 0
				self.CQdim = 0
				self.m_elemNum = 1
				self.status = 3
			self.ELEMNUM = self.ELEMNUM + self.m_elemNum
			self.COUNTER = self.COUNTER + 1
			if self.ELEMNUM > self.MAXELEMNUM:
				self.MAXELEMNUM = self.ELEMNUM
			if self.m_elemNum > self.MAXELEMTEN:
				self.MAXELEMTEN = self.m_elemNum

		except:
			raise

	## @brief Construct blocks related to Bonds and Qnums in UniTensor.

	## Calculate dimensions and offsets of Qnums in in-bonds and out-bonds.
	## Set default blocks respect to Qnums exist in both in-bonds and out-bonds.
	cdef int grouping(self):
		self.blocks.clear() ## Clear the blocks in UniTensor.
		
		row_bondNum = 0
		col_bondNum = 0
		self.RQdim = 1 ## Initialize the row dimension of quantum number.
		self.CQdim = 1 ## Initialize the column dimension of quantum number.
		IN_BONDS_BEFORE_OUT_BONDS = True ## Flag to check if all incoming bonds are before outcoming bonds in list bds.

		try:
			## Count numbers of in-bonds and out-bonds.
			## Check all in-bonds are before out-bonds.
			for i in range(len(self.bonds)): ## Run over all input bonds.
				if self.bonds[i].type() == "BD_IN": ## Check if the type of current bond is BD_IN.
					if IN_BONDS_BEFORE_OUT_BONDS == False: ## Check if all incoming bonds are before outcoming bonds.
						raise RuntimeError("Error in input bond list: BD_OUT bonds must be placed after all BD_IN bonds.")
					
					self.RQdim = self.RQdim * len(self.bonds[i].Qnums)
					row_bondNum = row_bondNum + 1

				else: ## The type of current bond is BD_OUT.
					self.CQdim = self.CQdim * len(self.bonds[i].Qnums)
					col_bondNum = col_bondNum + 1
					IN_BONDS_BEFORE_OUT_BONDS = False
			self.RBondNum = row_bondNum

			##-----------------------------------------------------------------------------
			## Declare variables.

			row_QnumMdim = {} ## Key:Qnum, Value:Dim. Total dimension of qnum of row bonds.
			row_offs = [0] * row_bondNum ## Offsets of row bonds.

			row_Qnum2Qidx = {} ## Key:Qnum, Value:Index. Index of qnums of row bonds.

			boff = 0 ## Qnum index.

			tmpRQidx2Dim = [1] * self.RQdim ## Dimension of qnums. The index of list is the same as index of qnums.
			tmpCQidx2Dim = [1] * self.CQdim

			tmpRQidx2Off = [0] * self.RQdim ## Offset of qnums. The index of list is the same as index of qnums.
			tmpCQidx2Off = [0] * self.CQdim

			##-----------------------------------------------------------------------------
			## Set dimension, index and offsets of qnums in row bonds.
			if row_bondNum > 0: ## Number of row bonds is not zero.
				while True:
					qnum = Qnum() ## Create a new Qnum obeject (U1=0, P=(0, 0)).
					dim = 1 ## Reset dim.
					for i in range(row_bondNum):
						qnum = qnum * self.bonds[i].Qnums[row_offs[i]]
						dim  = dim * self.bonds[i].Qdegs[row_offs[i]]

					if qnum in row_QnumMdim: ## The key "qnum" is already in dict row_QnumMdim.
						tmpRQidx2Off[boff] = row_QnumMdim[qnum] ## Set the offset of qnum corresponds to index of qnum.
						tmpRQidx2Dim[boff] = dim ## Set the dimension of qnum corresponds to index of qnum.
						row_QnumMdim[qnum] = row_QnumMdim[qnum] + dim
						row_Qnum2Qidx[qnum].append(boff) ## Add new index to list of index of the exist qnum.

					else: ## The key "qnum" is not in dict row_QnumMdim, which means qnum appears for the first time.
						tmpRQidx2Off[boff] = 0
						tmpRQidx2Dim[boff] = dim
						row_QnumMdim[qnum.copy()] = dim
						row_Qnum2Qidx[qnum.copy()] = [boff] ## Add new key qnum with its index.

					boff = boff + 1 ## Move to the next index.
					for i in range(row_bondNum-1, -2, -1):
						row_offs[i] = row_offs[i] + 1
						if row_offs[i] < len(self.bonds[i].Qnums):
							break
						else:
							row_offs[i] = 0
					if i < 0: ## Run over all row bond offsets.
						break
			else: ## No row bond, give defalut value to dimension and offset of qnum.
				qnum = Qnum() ## Create a new Qnum obeject (U1=0, P=(0, 0)).
				row_QnumMdim[qnum] = 1
				row_Qnum2Qidx[qnum] = [0]

			##-----------------------------------------------------------------------------
			## Declare variables.
			col_QnumMdim = {} ## Key:Qnum, Value:Dim. Total dimension of qnum of column bonds.
			col_offs = [0] * col_bondNum ## Offsets of column bonds.
			col_Qnum2Qidx = {} ## Key:Qnum, Value:Index. Index of qnums of column bonds.

			boff = 0 ## Reset qnum index.

			##-----------------------------------------------------------------------------
			## Set dimension, index and offsets of qnums in column bonds.
			if col_bondNum > 0: ## Number of column bonds is not zero.
				while True:
					qnum = Qnum() ## Reset qnum to (U1=0, P(0, 0))
					dim = 1 ## Reset dim to 1.
					for i in range(col_bondNum):
						qnum = qnum * self.bonds[i+row_bondNum].Qnums[col_offs[i]]
						dim = dim * self.bonds[i+row_bondNum].Qdegs[col_offs[i]]
	
					if qnum in row_QnumMdim: ## The key "qnum" is already in dict row_QnumMdim.
						if qnum in col_QnumMdim: ## The key "qnum" is also in dict col_QnumMdim.
							tmpCQidx2Off[boff] = col_QnumMdim[qnum] ## Set the offset of qnum corresponds to index of qnum.
							tmpCQidx2Dim[boff] = dim ## Set the dimension of qnum corresponds to index of qnum.
							col_QnumMdim[qnum] = col_QnumMdim[qnum] + dim
							col_Qnum2Qidx[qnum].append(boff) ## Add new index to the exist qnum.
	
						else: ## The key "qnum" is in row_QnumMdim but not in col_QnumMdim.
							tmpCQidx2Off[boff] = 0
							tmpCQidx2Dim[boff] = dim
							col_QnumMdim[qnum.copy()] = dim
							col_Qnum2Qidx[qnum.copy()] = [boff] ## Add new key qnum with its index.
	
					boff = boff + 1 ## Move to the next index.
					for i in range(col_bondNum-1, -2, -1):
						col_offs[i] = col_offs[i] + 1
						if col_offs[i] < len(self.bonds[i+row_bondNum].Qnums):
							break
						else:
							col_offs[i] = 0
					if i < 0: ## Run over all column bond offsets.
						break
			else: ## No row bond, give defalut value to dimension and offset of qnum.
				qnum = Qnum() ## Reset qnum to (U1=0, P(0, 0))
				if qnum in row_QnumMdim: ## Check if qnum exist in row bonds.
					col_QnumMdim[qnum] = 1
					col_Qnum2Qidx[qnum] = [0]

			##-----------------------------------------------------------------------------
			## Declare variable.
			off = 0
			Qidx = set() ## A list of qnum indice respect to the whole UniTensor.
			
			##-----------------------------------------------------------------------------
			## Construct blocks corresponding to qnums.
			## Set dimensions and offsets to UniTnesor attributes with respect to indices of qnums.
			for k2 in col_QnumMdim: ## Iterate over qnums in col_QnumMdim.
				k1 = Qnum(k2.U1(), k2.prt(), k2.prtF()) ## Qnum's in col_QnumMdim must exist in row_QnumMdim.
				blk = np.zeros(shape=(row_QnumMdim[k1], col_QnumMdim[k2])) ## Create a 2-D numpy array with size of (RQ_dim * CQ_dim) and default elements 0.
				off = off + blk.shape[0] * blk.shape[1]
				self.blocks[k1.copy()] = blk ## Add the Qnum:Block pair into dict blocks.
				tmpRQidx = row_Qnum2Qidx[k1] ## Get indices of qnum k1 in row bonds.
				tmpCQidx = col_Qnum2Qidx[k1] ## Get indices of qnum k1 in column bonds.

				for i in tmpRQidx: ## Run over all indices of qnum k1 in row bonds.
					self.RQidx2Blk[i] = self.blocks[k1] ## Set blk to the certain index of k1.
					for j in tmpCQidx: ## Run over all indices of qnum k1 in column bonds.
						## Set dimensions and offsets to UniTensor attributes.
						self.RQidx2Dim[i] = tmpRQidx2Dim[i]
						self.RQidx2Off[i] = tmpRQidx2Off[i]
						self.CQidx2Dim[j] = tmpCQidx2Dim[j]
						self.CQidx2Off[j] = tmpCQidx2Off[j]

						qidx = i * self.CQdim + j
						Qidx.add(qidx) ## A set of qnum indice.

			##-----------------------------------------------------------------------------
			## Set QidxEnc.
			elem_enc = 0
			for i in self.RQidx2Dim: ## Iterate over all indices of row bonds.
				for j in self.CQidx2Dim: ## Iterate over all indices of column bonds.
					qidx = i * self.CQdim + j
					if qidx in Qidx: ## qidx is in the set Qidx.
						self.QidxEnc[qidx] = elem_enc
						elem_enc = elem_enc + self.RQidx2Dim[i] * self.CQidx2Dim[j]
			return off
		except:
			raise
			return 0

	cdef list elemArray(self):
		bondNum = len(self.bonds)
		Q_Bdims = [0] * bondNum

		for i in range(bondNum):
			Q_Bdims[i] = len(self.bonds[i].Qnums)

		qnum_idxs = list(self.QidxEnc.keys())
		qnum_idxs.sort()
		elem = [0] * self.m_elemNum
		for i in qnum_idxs:
			RQoff = i // self.CQdim
			E_off = self.QidxEnc[i]
			blk_elem = self.RQidx2Blk[RQoff].flatten().tolist()
			for j, k in enumerate(blk_elem):
				elem[E_off+j] = k
		return elem

	## @brief Turn UniTensor object to string.

	## @Code:
	##**************** Demo ****************
	##     ____________
	##    |            |
	##0___|2          2|___2
	##    |            |
	##1___|3          3|___3
	##    |            |
	##    |____________|
	##
	##================BONDS===============
	##IN : (U1 = 0, P = 0, 0)|2, Dim = 2
	##IN : (U1 = 0, P = 0, 0)|1, (U1 = 1, P = 0, 0)|1, (U1 = 2, P = 0, 0)|1, Dim = 3
	##OUT : (U1 = 0, P = 0, 0)|2, Dim = 2
	##OUT : (U1 = 0, P = 0, 0)|1, (U1 = 1, P = 0, 0)|1, (U1 = 2, P = 0, 0)|1, Dim = 3
	##
	##===============BLOCKS===============
	##---(U1 = 0, P = 0, 0): 2 x 2 = 4, REAL
	##
	##            0.000            3.000
	##           18.000           21.000
	##
	##---(U1 = 1, P = 0, 0): 2 x 2 = 4, REAL
	##
	##			  7.000			  10.000
	##			 25.000			  28.000
	##
	##---(U1 = 2, P = 0, 0): 2 x 2 = 4, REAL
	##
	##			 14.000			  17.000
	##			 32.000			  35.000
	##
	##
	##Total elemNum : 12
	##***************** END ****************
	## @Encode:
	## In the above example, the UniTensor has four bonds with default labels [0, 1, 2, 3].
	## The bonds 0 and 1 are incoming, and 2, 3 are out-going bonds.
	## The bonds 0 and 2 have quantum number 0, with degeneracy of two, 
	## and 1, 3 have quantum number [0, 1, 2].
	## The blocks elements of the tensor are also shown.
	## There are three blocks of various U1 = 0, 1, 2 and same size.
	## The total element number is 12.
	def __str__(self):
		try:
			s = ""
			if (self.status == 3):
				s = "\nScalar: " + str(self.elem[0]) + "\n"
				return s

			bonds = self.bonds
			row = 0
			col = 0
			for i in bonds: ## Count size of row and column.
				if i.type() == "BD_IN":
					row = row + 1
				else:
					col = col + 1
			layer = max(row, col) ## Layer of block diagram of UniTensor.
			nmlen = len(self.name) + 2 ## Length of name of UniTensor.
			star = 12 + (14-nmlen) // 2 ## Number of star mark.

			s = s + '*' * star
			if len(self.name) > 0:
				s = s + ' ' + self.name + ' '
			s = s + '*' * star + '\n'

			sampleBlk = list(self.blocks.values())[0] ## To check the data type of UniTensor.
			if sampleBlk.dtype.name == "float64":
				s = s + "REAL\n"
			elif sampleBlk.dtype.name == "complex128":
				s = s + "COMPLEX\n"

			s = s + "\n             ____________\n"
			s = s + "            |            |\n"
			llab = 0 ## Bond label on the left hand side of block diagram.
			rlab = 0 ## Bond label on the right hand side of block diagram.

			for i in range(layer): ## Block diagram.
				if i < row and i < col:
					llab = self.labels[i]
					rlab = self.labels[row+i]
					s = s + "    {:>5}___|{:<4}    {:>4}|___{:<5}\n".format(llab, bonds[i].dim(), bonds[row+i].dim(), rlab)
				elif i < row:
					llab = self.labels[i]
					s = s + "    {:>5}___|{:<4}    {:>4}|\n".format(llab, bonds[i].dim(), "")
				elif i < col:
					rlab = self.labels[row+i]
					s = s + "    {:>5}   |{:<4}    {:>4}|___{:<5}\n".format("", "", bonds[row+i].dim(), rlab)
				s = s + "            |            |   \n"
			s = s + "            |____________|\n"

			s = s + "\n================BONDS===============\n"
			for i in bonds: ## Print bonds in UniTensor.
				s = s + str(i) + '\n'

			s = s + "\n===============BLOCKS===============\n"
			blocks = self.getBlocks()
			printElem = True
			blk_list = list(self.blocks.keys())
			blk_list.sort()
			for k1 in blk_list: ## Print blocks in UniTensor.
				s = s + "---" + str(k1) + ": "
				blk = blocks[k1]

				if (self.status == 4) and printElem:
					s = s + "{0} x {1} = {2}".format(blk.shape[0], blk.shape[1], blk.size)
					if blk.dtype.name == "float64":
						s = s + ", REAL\n\n"

						for i in range(blk.shape[0]):
							for j in range(blk.shape[1]):
								s = s + "{0:>17.3f}".format(blk[i][j])
							s = s + "\n"
						s = s + "\n"

					elif blk.dtype.name == "complex128":
						s = s + ", COMPLEX\n\n"

						for i in range(blk.shape[0]):
							for j in range(blk.shape[1]):
								s = s + "{0:>17.3f}".format(blk[i][j])
							s = s + "\n"
						s = s + "\n"
				else:
					s = s + "{0} x {1} = {2}\n\n".format(blk.shape[0], blk.shape[1], blk.size)

			s = s + "\n"
			s = s + "Total elemNum : {0}\n".format(self.m_elemNum)
			s = s + "***************** END ****************\n\n"
			return s

		except:
			raise

	## @brief Perform element-wise addition.

	## Perform element-wise addition. The tensor _Tb to be added must be similar to _Ta.
	## @param _Ta	First tensor.
	## @param _Tb	Second tensor. 
	def __add__(UniTensor _Ta, UniTensor _Tb):
		try:
			Ta = _Ta.copy()
			Tb = _Tb.copy()

			if not (Ta.status >= 3 and Tb.status >= 3):
				raise RuntimeError("Cannot perform addition of tensors before setting their elements.")
			if Ta.status == 3 and Tb.status == 3: ## Both Ta and Tb are scalar.
				if isinstance(Ta.elem[0], complex) and isinstance(Tb.elem[0], float):
					Tb.elem[0] = complex(Tb.elem[0])
				elif isinstance(Ta.elem[0], float) and isinstance(Tb.elem[0], complex):
					Ta.elem[0] = complex(Ta.elem[0])
			elif Ta.status == 4 and Tb.status == 4:
				smpBlkA = Ta.RQidx2Blk[0]
				smpBlkB = Tb.RQidx2Blk[0]
				if smpBlkA.dtype.name == "float64" and smpBlkB.dtype.name == "complex128":
					RtoC(smpBlkA)
				elif smpBlkA.dtype.name == "complex128" and smpBlkB.dtype.name == "float64":
					RtoC(smpBlkB)

			Tc = Ta.copy()
			if Ta.status == 3 and Tb.status == 3:
				Tc.elem[0] = Ta.elem[0] + Tb.elem[0]
			else:
				for q in Tc.blocks:
					newBlk = Ta.blocks[q] + Tb.blocks[q]
					Tc.putBlock(q, newBlk)

			return Tc

		except:
			raise

	def __mul__(_Ta, _Tb):
		try:
			if isinstance(_Ta, UniTensor) and isinstance(_Tb, UniTensor):
				Ta = _Ta.copy()
				Tb = _Tb.copy()
				return contract(Ta, Tb, True)
	
			elif isinstance(_Ta, UniTensor):
				if isinstance(_Tb, int) or isinstance(_Tb, float):
					Ta = _Ta.copy()
					for q in Ta.blocks:
						newBlk = Ta.blocks[q].copy()
						newBlk = newBlk * _Tb
						Ta.putBlock(q, newBlk)
	
				elif isinstance(_Tb, complex):
					Ta = _Ta.copy()
					RtoC(Ta)
					for q in Ta.blocks:
						newBlk = Ta.blocks[q].copy()
						newBlk = newBlk * _Tb
						Ta.putBlock(q, newBlk)
				return Ta
	
			elif isinstance(_Tb, UniTensor):
				if isinstance(_Ta, int) or isinstance(_Ta, float) or isinstance(_Ta, complex):
					return _Tb * _Ta
		
		except:
			raise

	def __call__(self):
		print(self)

	## @brief Access labels of Bonds in UniTensor.

	## Can access either the whole list of labels or a single label.
	## The default value of idx is -1, which returns the list of labels.
	## @param idx    Index of label.
	## @return Labels, as list or integer.
	cpdef label(self, int idx=-1):
		try:
			if idx == -1:
				return self.labels[:]
			elif idx >= len(self.labels):
				raise RuntimeError("Index exceeds the number of the bonds({0}).".format(len(self.labels)))
			else:
				return self.labels[idx]
		except:
			raise

	## @brief Copy tensor.

	## Create new object with same information of the input tensor.
	## @return A copy of input tensor.
	cpdef UniTensor copy(self):
		bonds = []
		for i in self.bonds:
			bd = i.copy()
			bonds.append(bd)
		UniTout = UniTensor(bonds, self.name)

		if not (self.status >= 3):
			return UniTout

		qnums = list(self.blocks.keys())
		for i in qnums:
			blk = self.blocks[i].copy()
			UniTout.putBlock(i, blk)

		UniTout.setLabel(self.labels)

		return UniTout

	## @brief Access name of UniTensor.

	## @return Name of UniTensor, as a string.
	cpdef str getName(self):
		return self._name

	## @brief Access number of Bonds in UniTensor.

	## @return Number of Bonds, as an integer.
	cpdef int bondNum(self):
		return len(self.bonds)

	## @brief Access number of in-bonds in UniTensor.

	## @return Number of in-bonds, as an integer.
	cpdef int inBondNum(self):
		return self.RBondNum

	## @brief Access Bonds in UniTensor.

	## Can access either the whole list of Bonds or a single Bond.
	## The default value of idx is -1, which returns the whole list of Bonds.
	## @param idx    Index of bond.
	## @return Bonds, as list or a Bond object.
	cpdef bond(self, int idx=-1):
		try:
			if idx == -1:
				bds = []
				for i in self.bonds:
					bds.append(i.copy())
				return bds
			elif idx >= len(self.bonds):
				raise RuntimeError("Index exceeds the number of the bonds({0}).".format(len(self.bonds)))
			else:
				return self.bonds[idx].copy()
		except:
			raise

	## @brief Access the number of elements.

	## Returns the total number of elements of the blocks.
	## @return Number of elements, as integer.
	cpdef int elemNum(self):
		return self.m_elemNum

	## @brief Access number of blocks.

	## @return Number of blocks, as integer.
	cpdef int blockNum(self):
		return len(self.blocks)

	## @brief Access block quantum numbers.

	## Can access quantum number of either all blocks or a single block.
	## The default value of idx is -1, which returns quantum number of all blocks.
	## @param idx     Index of block.
	## @return Quantum number, as list or Qnum object.
	cpdef blockQnum(self, int idx=-1):
		try:
			if idx == -1:
				Qnum_list = []
				for i in list(self.blocks.keys()):
					Qnum_list.append(i.copy())
			elif idx >= len(self.blocks):
				raise RuntimeError("Index exceeds the number of the blocks({0}).".format(len(self.blocks)))
			else:
				return list(self.blocks.keys())[idx].copy()
		except:
			raise

	cpdef void assign(self, list _bond):
		try:
			self = UniTensor(_bond)
		except:
			raise

	## @brief Access block elemnets.

	## Returns the tensor element blocks of Qnums as a dictionary of Qnums as keys and corresponding blocks as values.
	## @return blocks, as dictionary.
	cpdef dict getBlocks(self):
		blocks = {}
		for q in self.blocks:
			blocks[q.copy()] = np.copy(self.blocks[q])
		return blocks

	## @brief Assign name.

	## Assign name to a UniTensor.
	## @param _name    Name to be assigned.
	cpdef void setName(self, str _name):
		self.name = _name

	## @brief Assign raw elements.

	## Assign raw elements in numpy ndarray rawElem to UniTensor.
	## This function will reorganize the raw elements into block-diagonal form.
	## @param rawElem    Numpy 1d array of raw elemnets.
	## @param changeType    A flag, if the value is False, means the dtype of existing blocks will not
	##                      change in this method, vice versa.
	cpdef void setRawElem(self, np.ndarray rawElem):
		try:
			if (self.status == 1 or self.status == 3):
				raise RuntimeError("Setting elements to a tensor without bonds is not supported.")
			
			sampleBlk = list(self.blocks.values())[0]
			RTypes = [np.int32, np.int64, np.float64]
			CTypes = [np.complex64, np.complex128]
			
			if (rawElem.dtype in RTypes) and (sampleBlk.dtype in RTypes):
				rawElem = rawElem.astype(np.float64)
			elif (rawElem.dtype in CTypes) and (sampleBlk.dtype in CTypes):
				rawElem = rawElem.astype(np.complex128)
			elif (rawElem.dtype in RTypes) and (sampleBlk.dtype in CTypes):
				rawElem = rawElem.astype(np.complex128)
			else:
				raise RuntimeError("Cannot convert Complex block into Real.")
			##if rawElem.dtype.name != sampleBlk.dtype.name:
			##	raise RuntimeError("Type of input block is incosistent with existing blocks : type({0}).".format(sampleBlk.dtype.name))

			bondNum = len(self.bonds)
			Q_idxs = [0] * bondNum ## The indices of quantum numbers of bonds in UniTensor.
			Q_Bdims = [0] * bondNum ## The qunatum number dimension of bonds in UniTensor.
			sB_idxs = [0] * bondNum
			sB_sBdims = [0] * bondNum
			rAcc = [1] * bondNum

			for i in range(bondNum):
				Q_Bdims[i] = len(self.bonds[i].Qnums)
			for i in range(bondNum-1, 0, -1):
				rAcc[i-1] = rAcc[i] * self.bonds[i].dim()

			Qnum_idxs = list(self.QidxEnc.keys())
			Qnum_idxs.sort()

			for i in Qnum_idxs:
				Q_off = i
				tmp = Q_off
				for j in range(bondNum-1, -1, -1):
					Q_idxs[j] = tmp % Q_Bdims[j]
					tmp = tmp // Q_Bdims[j]

				R_off = 0
				for j in range(bondNum):
					R_off = R_off + rAcc[j] * self.bonds[j].offsets[Q_idxs[j]]
					sB_sBdims[j] = self.bonds[j].Qdegs[Q_idxs[j]]

				RQoff = Q_off // self.CQdim
				CQoff = Q_off % self.CQdim
				B_cDim = self.RQidx2Blk[RQoff].shape[1]
				E_off = self.QidxEnc[i]
				sB_rDim = self.RQidx2Dim[RQoff]
				sB_cDim = self.CQidx2Dim[CQoff]
				sB_idxs = [0] * bondNum
				RQnum_off = self.RQidx2Off[RQoff]
				CQnum_off = self.CQidx2Off[CQoff]

				for sB_r in range(sB_rDim):
					for sB_c in range(sB_cDim):
						##self.elem[E_off + (sB_r * sB_cDim) + sB_c] = rawElem[R_off]
						self.RQidx2Blk[RQoff][RQnum_off+sB_r][CQnum_off+sB_c] = rawElem[R_off]

						for k in range(bondNum-1, -1, -1):
							sB_idxs[k] = sB_idxs[k] + 1
							if sB_idxs[k] < sB_sBdims[k]:
								R_off = R_off + rAcc[k]
								break
							else:
								R_off = R_off - rAcc[k] * (sB_idxs[k]-1)
								sB_idxs[k] = 0


			self.status = 4

		except:
			raise

	## @brief Assign elements to a block.

	## Assign elements in block blk to the block of quantum number qnum in UniTensor.
	## This method will replace existing elements to elements in blk.
	## @param qnum    Specify block with quantum number qnum to replace elements.
	## @param blk    A block carries elements.
	cpdef void putBlock(self, Qnum qnum, np.ndarray blk):
		try:
			sampleBlk = list(self.blocks.values())[0]
			RTypes = [np.int32, np.int64, np.float64]
			CTypes = [np.complex64, np.complex128]
			
			if (blk.dtype in RTypes) and (sampleBlk.dtype in RTypes):
				blk = blk.astype(np.float64)
			elif (blk.dtype in CTypes) and (sampleBlk.dtype in CTypes):
				blk = blk.astype(np.complex128)
			elif (blk.dtype in RTypes) and (sampleBlk.dtype in CTypes):
				blk = blk.astype(np.complex128)
			else:
				raise RuntimeError("Cannot convert Complex block into Real.")

			##if blk.dtype.name != sampleBlk.dtype.name:
			##	raise RuntimeError("Type of input block is incosistent with existing blocks : type({0}).".format(sampleBlk.dtype.name))
			
			if qnum not in self.blocks: ## Check if qnum is in UniTensor.
				raise RuntimeError("There is no block with the given quantum number {0}.".format(str(qnum)))
			
			if (blk.shape[0], blk.shape[1]) != self.blocks[qnum].shape: ## The shape of input block and existing block should be same.
				s = "The dimension of input matrix does not match for the dimension of the block with quantum number ".format(str(qnum))
				s = s + "\n" + "Hint : Use numpy.reshape()."
				raise RuntimeError(s)

			np.place(self.blocks[qnum], np.ones(self.blocks[qnum].shape), blk.flatten())
			if(self.status == 3):
				self.elem[0] = blk.flatten()[0]
			elif(self.status == 2):
				self.status = 4

			##Qnum_idxs = list(self.QidxEnc.keys())
			##Qnum_idxs.sort()
			##for i in Qnum_idxs:
			##	RQ_off = i // self.CQdim
			##	E_off = self.QidxEnc[i]
			##	elem = self.RQidx2Blk[RQ_off].flatten().tolist()
			##	for j, k in enumerate(elem):
			##		self.elem[E_off+j] = k
		except:
			raise

	## @brief Permute the order of bonds.

	## Permutes the order of bonds to the order according to newLabels and rowBondNum(incoming bonds).
	## @param newLabels    list of new labels.
	## @param rowBondNum    Number of incoming bonds after permutation.
	cpdef UniTensor permute(self, list newLabels, int rowBondNum):
		try:
			if len(self.bonds) == 0:
				raise RuntimeError("There is no bond in the tensor to permute.")
			if not (len(self.labels) == len(newLabels)):
				raise RuntimeError("The size of input new labels does not match for the number of bonds.")

			newlab = newLabels[:]
			bondNum = len(self.bonds)
			rsp_outin = [0] * bondNum
			cnt = 0
			for i in range(bondNum):
				for j in range(bondNum):
					if self.labels[i] == newLabels[j]:
						rsp_outin[j] = i
						cnt = cnt + 1
			if not (cnt == len(newLabels)):
				raise RuntimeError("The input new labels do not 1-1 correspond to the labels of the tensor.")

			inorder = True
			for i in range(bondNum):
				if not (rsp_outin[i] == i):
					inorder = False
					break

			if inorder and rowBondNum == self.RBondNum: ## Do nothing
				UniTout = self.copy()
				UniTout.setLabel(newLabels)
				return UniTout

			else:
				outBonds = []
				withoutSymmetry = True
				for i in range(bondNum):
					outBonds.append(self.bonds[rsp_outin[i]])
					if not len(self.bonds[i].Qnums) == 1:
						withoutSymmetry = False
				for i in range(bondNum):
					if i < rowBondNum:
						outBonds[i] = outBonds[i].change("BD_IN")
					else:
						outBonds[i] = outBonds[i].change("BD_OUT")

				UniTout = UniTensor(outBonds, self.name)
				if self.status == 4:
					if withoutSymmetry:
						src_elem = self.RQidx2Blk[0].flatten().tolist()
						if not inorder:
							des_elem = [0] * len(src_elem)
							transAcc = [0] * bondNum
							newAcc = [0] * bondNum
							bondDims = [0] * bondNum
							idxs = [0] * bondNum
							transAcc[bondNum-1] = 1
							newAcc[bondNum-1] = 1

							for i in range(bondNum-1, 0, -1):
								newAcc[i-1] = newAcc[i] * UniTout.bonds[i].Qdegs[0]
							for i in range(bondNum):
								transAcc[rsp_outin[i]] = newAcc[i]
								bondDims[i] = self.bonds[i].Qdegs[0]

							cnt_ot = 0
							for i in src_elem:
								des_elem[cnt_ot] = i
								for j in range(bondNum-1, -1, -1):
									idxs[j] = idxs[j] + 1
									if idxs[j] < bondDims[j]:
										cnt_ot = cnt_ot + transAcc[j]
										break
									else:
										cnt_ot = cnt_ot - transAcc[j] * (idxs[j]-1)
										idxs[j] = 0
						else:
							des_elem = src_elem[:]
						row = UniTout.RQidx2Dim[0]
						col = UniTout.CQidx2Dim[0]
						for i in range(row):
							for j in range(col):
								UniTout.RQidx2Blk[0][i][j] = des_elem[i*col + j]


					else:
						sign = 1.0

						## For Fermionic system
						swap = []
						fermionic = False
						for i in self.bonds:
							for q in self.bonds.Qnums:
								if q.isFermionic():
									fermionic = True
						if fermionic:
							inLabelF = [0] * bondNum
							outLabelF = [0] * bondNum
							ordF = [0] * bondNum

							for i in range(self.RBondNum):
								inLabelF[i] = self.labels[i]
								ordF[i] = i

							for i in range(UniTout.RBondNum):
								outLabelF[i] = newLabels[i]

							for i in range(bondNum-1, self.RBondNum-1, -1):
								ordF[i] = bondNum - i + self.RBondNum - 1
								inLabelF[ordF[i]] = self.labels[i]

							for i in range(bondNum-1, UniTout.RBondNum-1, -1):
								outLabelF[bondNum - i + UniTout.RBondNum - 1] = newLabels[i]

							rspF_outin = [0] * bondNum
							for i in range(bondNum):
								for j in range(bondNum):
									rsp_outin[j] = i
							swaps = recSwap(rsp_outin, ordF)
						## End Fermionic system

						Qin_idxs = [0] * bondNum
						Qot_idxs = [0] * bondNum
						sBin_idxs = [0] * bondNum
						sBin_sBdims = [0] * bondNum
						Qot_acc = [1] * bondNum
						sBot_acc = [1] * bondNum
						for i in range(bondNum-1, 0, -1):
							Qot_acc[i-1] = Qot_acc[i] * len(UniTout.bonds[i].Qnums)
						
						Qnum_idxs = list(self.QidxEnc.keys())
						Qnum_idxs.sort()
						for i in Qnum_idxs:
							Qin_off = i
							tmp = Qin_off
							for j in range(bondNum-1, -1, -1):
								qdim = len(self.bonds[j].Qnums)
								Qin_idxs[j] = tmp % qdim
								sBin_sBdims[j] = self.bonds[j].Qdegs[Qin_idxs[j]]
								tmp = tmp // qdim

							Qot_off = 0
							for j in range(bondNum):
								Qot_idxs[j] = Qin_idxs[rsp_outin[j]]
								Qot_off = Qot_off + Qot_idxs[j] * Qot_acc[j]

							for j in range(bondNum-1, 0, -1):
								sBot_acc[rsp_outin[j-1]] = sBot_acc[rsp_outin[j]] * self.bonds[rsp_outin[j]].Qdegs[Qot_idxs[j]]

							Qin_RQoff = Qin_off // self.CQdim
							Qin_CQoff = Qin_off % self.CQdim
							Qot_RQoff = Qot_off // UniTout.CQdim
							Qot_CQoff = Qot_off % UniTout.CQdim
							Bin_cDim = self.RQidx2Blk[Qin_RQoff].shape[1]
							Bot_cDim = UniTout.RQidx2Blk[Qot_RQoff].shape[1]
							Ein_off = self.QidxEnc[i]
							Eot_off = UniTout.QidxEnc[i]
							sBin_rDim = self.RQidx2Dim[Qin_RQoff]
							sBin_cDim = self.CQidx2Dim[Qin_CQoff]
							sBot_cDim = UniTout.CQidx2Dim[Qot_CQoff]
							cnt_ot = 0
							sBin_idxs = [0] * bondNum
							RQin_off = self.RQidx2Off[Qin_RQoff]
							CQin_off = self.RQidx2Off[Qin_CQoff]
							RQot_off = UniTout.RQidx2Off[Qot_RQoff]
							CQot_off = UniTout.CQidx2Off[Qot_CQoff]

							if fermionic:
								sign01 = 0
								for i in range(len(swaps)):
									sign01 = sign01 ^ (self.bonds[swaps[i].b1].Qnums[Qin_idxs[swaps[i].b1]].m_prtF & self.bonds[swaps[i].b2].Qnums[Qin_idxs[swaps[i].b2]].m_prtF)
									if sign01:
										sign = -1.0
									else:
										sign = 1.0
							for j in range(sBin_rDim):
								for k in range(sBin_cDim):
									sBot_r = cnt_ot // sBot_cDim
									sBot_c = cnt_ot % sBot_cDim
									##UniTout.elem[Eot_off + (sBot_r * sBot_cDim) + sBot_c] = sign * self.elem[Ein_off + (j * Bin_cDim) + k]
									##UniTout.RQidx2Blk[Qot_RQoff][RQot_off + sBot_r][CQot_off + sBot_c] = sign * self.elem[Ein_off + (j * sBin_cDim) + k]
									UniTout.RQidx2Blk[Qot_RQoff][RQot_off+sBot_r][CQot_off+sBot_c] = sign * self.RQidx2Blk[Qin_RQoff][RQin_off+j][CQin_off+k]
									for bend in range(bondNum-1, -1, -1):
										sBin_idxs[bend] = sBin_idxs[bend] + 1
										if sBin_idxs[bend] < sBin_sBdims[bend]:
											cnt_ot = cnt_ot + sBot_acc[bend]
											break
										else:
											cnt_ot = cnt_ot - sBot_acc[bend] * (sBin_idxs[bend] - 1)
											sBin_idxs[bend] = 0

					UniTout.status = 4

				UniTout.setLabel(newlab)
				return UniTout

		except:
			raise

	## @brief Assign labels to bonds in UniTensor.

	## Assigns labels to bonds or a label to a bond indicated by index idx, replacing the origin labels.
	## The default value of index idx is -1, which means replace all labels in UniTensor.
	## @param label    New labels, as an integer or a list of integers.
	## @param idx    Index, as an integer.
	cpdef void setLabel(self, label, int idx = -1):
		try:
			if idx == -1:
				if type(label) != list:
					raise TypeError("Type of input label should be list when idx = -1.")
				else:
					if len(self.bonds) != len(label):
						raise RuntimeError("The size of input list(label) does not match for the number of bonds.")
					else:
						self.labels = label[:]
			elif len(self.bonds) <= idx:
				raise RuntimeError("The bond index is out of range of the list of labels.")
			else:
				self.labels[idx] = label
		except:
			raise

	cpdef np.ndarray getBlock(self, Qnum qnum=Qnum()):
		try:
			if qnum not in self.blocks:
				raise RuntimeError("There is no block with the given quantum number {0}", format(qnum))
			
			mat = np.copy(self.blocks[qnum])
			return mat
		except:
			raise

	cpdef void printDiagram(self):
		try:
			if self.status == 3:
				s = "\n" + str(self.elem[0]) + "\n\n"

			else:
				row = 0
				col = 0
				bondNum = len(self.bonds)
				for i in self.bonds:
					if i.type() == "BD_IN":
						row = row + 1
					else:
						col = col + 1

				layer = max(row, col)
				nmlen = len(self.name) + 2
				star = 12 + (14-nmlen)//2

				s = "*"*star
				if len(self.name) > 0:
					s = s + " " + self.name + " "
				s = s + "*"*star + "\n"

				if self.RQidx2Blk[0].dtype.name == "float64":
					s = s + " REAL \n"
				elif self.RQidx2Blk[0].dtype.name == "complex128":
					s = s + " COMPLEX \n"

				s = s + "\n             ____________\n"
				s = s + "            |            |\n"
				
				llab = 0
				rlab = 0
				for i in range(layer):
					if i < row and i < col:
						llab = self.labels[i]
						rlab = self.labels[row+i]
						s = s + "    {:>5}___|{:<4}    {:>4}|___{:<5}\n".format(llab, self.bonds[i].dim(), self.bonds[row+i].dim(), rlab)
					elif i < row:
						llab = self.labels[i]
						s = s + "    {:>5}___|{:<4}    {:>4}|\n".format(llab, self.bonds[i].dim(), "")
					elif i < col:
						rlab = self.labels[row+i]
						s = s + "    {:>5}   |{:<4}    {:>4}|___{:<5}\n".format("", "", self.bonds[row+i].dim(), rlab)
					s = s + "            |            |   \n"
				s = s + "            |____________|\n"

				s = s + "\n================BONDS===============\n"
				for i in self.bonds:
					s = s + str(i) + "\n"
				s = s + "***************** END ****************\n\n"
				print(s)

		except:
			raise

	cpdef getElemAt(self, int idx):
		elem = self.elemArray()
		return elem[idx]

	cpdef at(self, list idxs=[]):
		try:
			if not(len(idxs) == len(self.bonds)):
				raise RuntimeError("The size of input indices array does not match with the number of the bonds.")
			if len(self.bonds) == 0:
				return self.elem[0]

			bondNum = len(self.bonds)
			Qidxs = [0] * bondNum
			for i in range(bondNum):
				if not(idxs[i] < self.bonds[i].dim()):
					raise RuntimeError("The input indices are out of range.")
				for j in range(len(self.bonds[i].offsets)-1, -1, -1):
					if idxs[i] < self.bonds[i].offsets[j]:
						continue
					Qidxs[i] = j
					break

			Q_acc = [1] * bondNum
			for i in range(bondNum-1, 0, -1):
				Q_acc[i-1] = Q_acc[i] * len(self.bonds[i].Qnums)

			Qoff = 0
			for i in range(bondNum):
				Qoff = Qoff + Q_acc[i]*Qidxs[i]

			if Qoff in self.QidxEnc:
				Q_RQoff = Qoff // self.CQdim
				Q_CQoff = Qoff % self.CQdim
				blk = self.RQidx2Blk[Q_RQoff]
				B_cDim = blk.shape[1]
				sB_cDim = self.CQidx2Dim[Q_CQoff]
				blkRoff = self.RQidx2Off[Q_RQoff]
				blkCoff = self.CQidx2Off[Q_CQoff]
				elem = self.elemArray()
				boff = self.QidxEnc[Qoff] + blkRoff*B_cDim + blkCoff
				cnt = 0
				D_acc = [1] * bondNum
				for i in range(bondNum-1, 0, -1):
					D_acc[i-1] = D_acc[i] * self.bonds[i].Qdegs[Qidxs[i]]
				for i in range(bondNum):
					cnt = cnt + (idxs[i]-self.bonds[i].offsets[Qidxs[i]]) * D_acc[i]
				return elem[(cnt/sB_cDim)*B_cDim + cnt%sB_cDim]
			else:
				return 0.0

		except:
			raise

	#cpdef np.ndarray getRawElem(self):
	#	try:
	#		if (self.status & self.HAVEBOND) and (self.status & self.HAVEELEM):
	#			bondNum = len(self.bonds)
	#			rowNum = 1
	#			colNum = 1
	#			for i in self.bonds:
	#				if i.type() == "BD_IN":
	#					rowNum = rowNum * i.dim()
	#				else:
	#					colNum = colNum * i.dim()
	#
	#			elem = []
	#			Qnum_idxs = list(self.QidxEnc.keys())
	#			Qnum_idxs.sort()
	#			for i in Qnum_idxs:
	#				blk = self.RQidx2Blk[i]
	#				elem = elem + blk.flatten().tolist()
	#
	#			idxs = [0] * bondNum
	#			rawElem = []
	#			while True:
	#				rawElem.append()
	#
	#	except:
	#		raise

	#cpdef UniTensor combinebond(self, list cmbLabels):
	#	try:
	#		if (self.status & self.HAVEBOND) == 0:
	#			raise RuntimeError("There is no bond in the tensor to be combined.")
	#		if not (len(cmbLabels) > 1):
	#			return self
	#		
	#		labelNum = len(self.labels)
	#		rsp_labels = [0] * labelNum
	#		reduced_labels = [0] * (labelNum-len(cmbLabels)+1)
	#		marked = [0] * labelNum
	#		picked = [0] * len(cmbLabels)
	#		for i in range(len(cmbLabels)):
	#			for j in range(labelNum):
	#				if cmbLabels[i] == self.labels[j]:
	#					picked[i] = j
	#					marked[j] = 1
	#					break
	#
	#		mark = 0
	#		for i in range(len(marked)):
	#			if marked[j]:
	#				mark = mark + 1
	#		if not (mark == len(cmbLabels)):
	#			raise RuntimeError("The input labels do not match for the labels of the tensor.")
	#
	#		enc = 0
	#		enc_r = 0
	#		newBonds = []
	#		RBnum = 0
	#		for i in range(labelNum):
	#			if marked[i] and i == picked[0]:
	#				for j in range(len(cmbLabels)):
	#					rsp_labels[enc] = cmbLabels[j]
	#					enc = enc + 1
	#				tmpBonds = []
	#				for j in picked:
	#					tmpBonds.append(self.bonds[j])
	#				if self.bonds[picked[0]].type() == "BD_IN":
	#					RBnum = RBnum + len(picked)
	#				newBonds.append(combine(tmpBonds))
	#				reduced_labels[enc_r] = self.labels[i]
	#				enc_r = enc_r + 1
	#
	#			elif marked[i] == 0:
	#				rsp_labels[enc] = self.labels[i]
	#				reduced_labels[enc_r] = self.labels[i]
	#				if self.bonds[i].type() == "BD_IN":
	#					RBnum = RBnum + 1
	#				newBonds.append(self.bonds[i])
	#				enc_r = enc_r + 1
	#				enc = enc + 1
	#
	#		self.permute(rsp_labels, RBnum)
	#		Tout = UniTensor(newBonds)
	#		Tout.setLabel(reduced_labels)
	#
	#		if self.status & self.HAVEELEM:
	#
	#								

	#	except:
	#		raise

	cpdef UniTensor randomize(self):
		UniTout = self.copy()
		lab = self.labels[:]
		for i in UniTout.blocks:
			shape = UniTout.blocks[i].shape
			UniTout.blocks[i] = np.random.rand(shape[0], shape[1])
		if(self.status == 2 or self.status == 4):
			UniTout.status = 4
		elif(self.status == 3):
			UniTout.status = 3
		UniTout.setLabel(lab)
		return UniTout
	

######################
## Global functions ##
######################

## @brief Combine a list of Bonds.

## Combines all bonds in the list bds.
## Type of combined bond is the type of first bond in bds.
## @return A combined bond.
cpdef Bond combine(list bds):
	try:
		if len(bds) == 0:
			raise RuntimeError("There should be at least one bond in the input vector to be combined.")
		if len(bds) == 1:
			bd = <Bond?>bds[0].copy()
			return bd
		tp = bds[0].type()
		bd_num = len(bds)
		outBond1 = bds[bd_num-1].copy()
		outBond2 = bds[bd_num-2].copy()
		outBond2.change(tp)
		outBond2 = outBond2.combine(outBond1)
		for b in range(bd_num-2):
			if (b%2) == 0:
				outBond1 = bds[bd_num-3-b]
				outBond1.change(tp)
				outBond1 = outBond1.combine(outBond2)
			else:
				outBond2 = bds[bd_num-3-b]
				outBond2.change(tp)
				outBond2 = outBond2.combine(outBond1)
		if (bd_num%2) == 0:
			return outBond2
		else:
			return outBond1
	except:
		raise
		return Bond()

## @brief Perform contraction of UniTensor.

## Perform tensor contraction of Ta and Tb. It contracts out bonds with the same labels
## in Ta and Tb.

## @note In contrast to operator * (Ta * Tb), this function performs contraction
## without copying Ta and Tb. When the flag fast is set to True, the two tensors
## Ta and Tb are contracted without permuting back to the original tensor.
## @return Tensor after contraction.
## @param Ta    First tensor.
## @param Tb    Second tensor.
## @param fast    A flag to set if Ta and Tb to be permuted back to the original ones
##				  after contraction. Default value is False.
cpdef UniTensor contract(UniTensor Ta, UniTensor Tb, bint fast=False):
	try:
		## Check element type of Ta and Tb.
		smpBlkA = Ta.RQidx2Blk[0]
		smpBlkB = Tb.RQidx2Blk[0]
		if smpBlkA.dtype.name == "float64" and smpBlkB.dtype.name == "complex128":
			RtoC(Ta)
		elif smpBlkA.dtype.name == "complex128" and smpBlkB.dtype.name == "float64":
			RtoC(Tb)

		if not (Ta.status & Tb.status & Ta.HAVEELEM):
			raise RuntimeError("Cannot perform contraction of two tensors before setting their elements.")

		## Skip Ta == Tb

		if (Ta.status == 4) and (Tb.status == 4):
			AbondNum = len(Ta.bonds)
			BbondNum = len(Tb.bonds)
			oldLabelA = Ta.labels[:]
			oldLabelB = Tb.labels[:]
			oldRnumA = Ta.RBondNum
			oldRnumB = Tb.RBondNum
			interLabel = []
			newLabelA = []
			newLabelB = []
			newLabelC = []
			markB = [0] * BbondNum
			for i in range(AbondNum):
				match = False
				for j in range(BbondNum):
					if Ta.labels[i] == Tb.labels[j]:
						markB[j] = 1
						interLabel.append(Ta.labels[i])
						newLabelB.append(Tb.labels[j])
						if not (Ta.bonds[i].dim() == Tb.bonds[j].dim()):
							raise RuntimeError("Cannot contract two bonds having different dimensions.")

						match = True
						break

				if not match:
					newLabelA.append(Ta.labels[i])
					newLabelC.append(Ta.labels[i])

			for i in interLabel:
				newLabelA.append(i)
			for j in range(BbondNum):
				if markB[j] == 0:
					newLabelB.append(Tb.labels[j])
					newLabelC.append(Tb.labels[j])

			conBond = len(interLabel)
			Ta = Ta.permute(newLabelA, AbondNum-conBond)
			Tb = Tb.permute(newLabelB, conBond)

			cBonds = []
			for i in range(AbondNum-conBond):
				cBonds.append(Ta.bonds[i].copy())
				
			for i in range(conBond, BbondNum):
				cBonds.append(Tb.bonds[i].copy())

			Tc = UniTensor(cBonds)
			if not len(Tc.bonds) == 0:
				Tc.setLabel(newLabelC)
			Tc = Tc.permute(newLabelC, AbondNum-conBond)

			for k1 in Ta.blocks:
				if k1 in Tb.blocks:
					k2 = Qnum(k1.U1(), k1.prt(), k1.prtF())
					blockA = Ta.blocks[k1]
					blockB = Tb.blocks[k2]
					blockC = Tc.blocks[k1]
					if not ((blockA.shape[0] == blockC.shape[0]) and (blockB.shape[1] == blockC.shape[1]) and (blockA.shape[1] == blockB.shape[0])):
						raise RuntimeError("The dimensions the bonds to be contracted out are different.")
					blk = np.dot(blockA, blockB)
					Tc.putBlock(k1, blk)
					if not(Tc.status & Tc.HAVEBOND):
						blk = np.dot(blockA, blockB)
						Tc.elem[0] = blk.flatten()[0]
			Tc.status = 4

			## Outer product.
			if conBond == 0:
				idx = 0
				for i in range(oldRnumA):
					newLabelC[idx] = oldLabelA[i]
					idx = idx + 1
				for i in range(oldRnumB):
					newLabelC[idx] = oldLabelB[i]
					idx = idx + 1
				for i in range(oldRnumA, AbondNum):
					newLabelC[idx] = oldLabelA[i]
					idx = idx + 1
				for i in range(oldRnumB, BbondNum):
					newLabelC[idx] = oldLabelB[i]
					idx = idx + 1
				Tc = Tc.permute(newLabelC, oldRnumA+oldRnumB)

			if not fast:
				Ta = Ta.permute(oldLabelA, oldRnumA)
				Tb = Tb.permute(oldLabelB, oldRnumB)
			return Tc

		elif (Ta.status == 4):
			return Ta * Tb.elem[0]
		elif (Tb.status == 4):
			return Tb * Ta.elem[0]

	except:
		raise

## @brief Tensor product of two tensors.

## Perform tensor product of Ta and Tb.
## @param Ta, Tb    Tensors to perform tensor product.
cpdef UniTensor otimes(UniTensor Ta, UniTensor Tb):
	try:
		T1 = Ta.copy()
		T2 = Tb.copy()
		label1 = [0] * T1.bondNum()
		label2 = [0] * T2.bondNum()
		for i in range(T1.bondNum()):
			if i < T1.inBondNum():
				label1[i] = i
			else:
				label1[i] = T2.inBondNum() + i
		for i in range(T2.bondNum()):
			if i < T2.inBondNum():
				label2[i] = i + T1.inBondNum()
			else:
				label2[i] = i + T1.bondNum()
		T1.setLabel(label1)
		T2.setLabel(label2)
		return contract(T1, T2, True)
	except:
		raise

## @brief Reshape numpy 2d array.

## Change the shape of a numpy array.
## The original array would be cut or filled by zeros if assigned
## size(assign.row*assign.col) does not match the original one.
## @param shape    New shape.
cpdef np.ndarray reshape(np.ndarray blk, tuple shape):
	try:	
		if (shape[0]*shape[1]) == blk.size:
			blk = blk.reshape(shape)
			return blk
		if shape[0] > blk.shape[0]:
			z = np.zeros(shape=(shape[0]-blk.shape[0], blk.shape[1]), dtype=blk.dtype)
			blk = np.concatenate((blk, z), axis=0)
		else:
			blk = blk[:shape[0]]
		if shape[1] > blk.shape[1]:
			z = np.zeros(shape=(blk.shape[0], shape[1]-blk.shape[1]), dtype=blk.dtype)
			blk = np.concatenate((blk, z), axis=1)
		else:
			blk = blk.T
			blk = blk[:shape[1]].T
		return blk
	except:
		raise

###########
## Tools ##
###########

cdef void RtoC(UniTensor UniT):
	try:
		sampleBlk = list(UniT.blocks.values())[0]
		if sampleBlk.dtype.name == "float64":
			for q in UniT.blocks:
				tmp_id = id(UniT.blocks[q])
				UniT.blocks[q] = UniT.blocks[q].astype(np.complex128)
				for i in UniT.RQidx2Blk:
					if tmp_id == id(UniT.RQidx2Blk[i]):
						UniT.RQidx2Blk[i] = UniT.blocks[q]
			UniT.elem[0] = complex(UniT.elem[0])

	except:
		raise

cdef list recSwap(list _ord, list ordF):
	ord = _ord
	n = len(_ord)
	cdef _Swap sg
	swaps = []
	for i in range(n-1):
		for j in range(n-i-1):
			if ord[j] > ord[j+1]:
				sg.b1 = ordF[ord[j+1]]
				sg.b2 = ordF[ord[j]]
				tmp = ord[j]
				ord[j] = ord[j+1]
				ord[j+1] = tmp
				swaps.append(sg)
	return swaps

#############
## Structs ##
#############

cdef struct _Swap:
	int b1
	int b2