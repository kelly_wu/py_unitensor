from pyUni10 import *
import unittest

class TestUniTensorMethods(unittest.TestCase):
	def test_DefaultConstructor(self):
		A = UniTensor()
		self.assertEqual(0, A.bondNum())

	def test_Constrictor(self):
		q1 = Qnum(1)
		bd = Bond("BD_IN", [q1]*10)
		A = UniTensor([bd])
		self.assertEqual(1, A.bondNum())

if __name__ == "__main__":
	unittest.main()