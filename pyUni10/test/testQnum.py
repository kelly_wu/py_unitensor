import unittest
from pyUni10 import Qnum

class TestQnumMethods(unittest.TestCase):
	def test_DefaultConstructor(self):
		q = Qnum()
		self.assertEqual(0, q.U1())
		self.assertEqual("PRT_EVEN", q.prt())
		self.assertEqual("PRTF_EVEN", q.prtF())
		self.assertFalse(q.isFermionic())

	def test_Constructor(self):
		q = Qnum(48763, "PRT_ODD", "PRTF_ODD")
		self.assertEqual(48763, q.U1())
		self.assertEqual("PRT_ODD", q.prt())
		self.assertEqual("PRTF_ODD", q.prtF())
		self.assertTrue(q.isFermionic())

	def test_ConstructorExcept(self):
		with self.assertRaises(TypeError):
			q = Qnum(0, 1)
		with self.assertRaises(TypeError):
			q = Qnum(0, "PRT_ODD", 1)
		with self.assertRaises(ValueError):
			q = Qnum(0, "A")
		with self.assertRaises(ValueError):
			q = Qnum(0, "PRT_ODD", "A")

	def test_Assign(self):
		q = Qnum()

		q.assign(3)
		self.assertEqual(3, q.U1())
		self.assertEqual("PRT_EVEN", q.prt())

		q.assign(0, "PRT_ODD")
		self.assertEqual(0, q.U1())
		self.assertEqual("PRT_ODD", q.prt())

		q.assign(-1)
		## Default value "PRT_EVEN" assigned
		self.assertEqual("PRT_EVEN", q.prt())
		self.assertEqual(-1, q.U1())

		q.assign(1, "PRT_ODD", "PRTF_EVEN")
		self.assertEqual(1, q.U1())
		self.assertEqual("PRT_ODD", q.prt())
		self.assertEqual("PRTF_EVEN", q.prtF())

		q.assign(prtF="PRTF_ODD")
		self.assertEqual("PRTF_ODD", q.prtF())

	def test_Hash(self):
		q = Qnum(100, "PRT_EVEN", "PRTF_ODD")
		self.assertEqual(401, q.hash())

	def test_OperationNegation(self):
		q1 = Qnum(-1)
		q1 = -q1
		self.assertEqual(1, q1.U1())

		q2 = Qnum(3, "PRT_ODD", "PRTF_EVEN")
		q2 = -q2
		self.assertEqual(-3, q2.U1())
		self.assertEqual("PRT_ODD", q2.prt())
		self.assertEqual("PRTF_EVEN", q2.prtF())

	def test_OperationLargeThan(self):
		q1 = Qnum(3)
		q2 = Qnum(-1)
		self.assertTrue(q1>q2)

		q1.assign(prtF="PRTF_ODD")
		q2.assign(prtF="PRTF_EVEN")
		self.assertTrue(q1>q2)

		q1.assign(prt="PRT_ODD")
		q2.assign(prt="PRT_EVEN")
		self.assertTrue(q1>q2)

	def test_OperationLessThan(self):
		q1 = Qnum(3)
		q2 = Qnum(-1)
		self.assertTrue(q2<q1)

		q1.assign(prtF="PRTF_ODD")
		q2.assign(prtF="PRTF_EVEN")
		self.assertTrue(q2<q1)

		q1.assign(prt="PRT_ODD")
		q2.assign(prt="PRT_EVEN")
		self.assertTrue(q2<q1)

	def test_OperationLargeEq(self):
		q1 = Qnum(3)
		q2 = Qnum(-1)
		self.assertTrue(q1>=q2)

		q1.assign(-1)
		self.assertTrue(q1>=q2)

	def test_OperationLessEq(self):
		q1 = Qnum(3)
		q2 = Qnum(-1)
		self.assertTrue(q2<=q1)

		q1.assign(-1)
		self.assertTrue(q2<=q1)

	def test_OperationEq(self):
		q1 = Qnum(3)
		q2 = Qnum(2)
		self.assertFalse(q1==q2)

		q1.assign(0, "PRT_ODD")
		q2.assign(0, "PRT_EVEN")
		self.assertFalse(q1==q2)

		q1.assign(0, "PRT_EVEN", "PRTF_ODD")
		q2.assign(0, "PRT_EVEN", "PRTF_EVEN")
		self.assertFalse(q1==q2)

	def test_OperationNotEq(self):
		q1 = Qnum(3)
		q2 = Qnum(2)
		self.assertTrue(q1!=q2)

		q1.assign(0, "PRT_ODD")
		q2.assign(0, "PRT_EVEN")
		self.assertTrue(q1!=q2)

		q1.assign(0, "PRT_EVEN", "PRTF_ODD")
		q2.assign(0, "PRT_EVEN", "PRTF_EVEN")
		self.assertTrue(q1!=q2)

	def test_OperationMul(self):
		q1 = Qnum(3, "PRT_ODD", "PRTF_EVEN")
		q2 = Qnum(2, "PRT_ODD", "PRTF_ODD")
		q3 = q1 * q2
		self.assertEqual(5, q3.U1())
		self.assertEqual("PRT_EVEN", q3.prt())
		self.assertEqual("PRTF_ODD", q3.prtF())

		q4 = Qnum(-1, "PRT_ODD")
		q5 = Qnum(2, "PRT_ODD")
		q3 = q4 * q5
		self.assertEqual(1, q3.U1())
		self.assertEqual("PRT_EVEN", q3.prt())
		self.assertNotEqual("PRTF_ODD", q3.prtF())

if __name__ == "__main__":
	unittest.main()