import unittest
from pyUni10 import Qnum, Bond

class TestBondMethods(unittest.TestCase):
	def test_Constructor(self):
		q1 = Qnum(1)
		q0 = Qnum(0)
		q_1 = Qnum(-1)
		qnums = [q1, q1, q0, q0, q0, q_1]

		bd = Bond("BD_OUT", qnums)
		self.assertEqual(6, bd.dim())

		qlist = bd.Qlist()
		self.assertEqual(q1, qlist[0])
		self.assertEqual(q1, qlist[1])
		self.assertEqual(q0, qlist[2])
		self.assertEqual(q0, qlist[3])
		self.assertEqual(q0, qlist[4])
		self.assertEqual(q_1, qlist[5])

		degs = bd.degeneracy()
		self.assertEqual(3, degs[q0])
		self.assertEqual(2, degs[q1])
		self.assertEqual(1, degs[q_1])

	def test_ChangeBondType(self):
		q = Qnum(1)
		qnums = [q] * 100
		bd = Bond("BD_IN", qnums)
		bd.change("BD_OUT")
		self.assertEqual("BD_OUT", bd.type())

		for i in bd.Qlist():
			self.assertEqual(-1, i.U1())

	def test_DummyChangeBondType(self):
		q = Qnum(1)
		qnums = [q] * 100
		bd = Bond("BD_IN", qnums)
		bd.dummy_change("BD_OUT")
		self.assertEqual("BD_OUT", bd.type())

		for i in bd.Qlist():
			self.assertEqual(1, i.U1())

	def test_Combine(self):
		q2 = Qnum(2)
		q1 = Qnum(1)
		q0 = Qnum()
		q_1 = Qnum(-1)
		q_2 = Qnum(-2)

		qnums = [q1, q1, q0, q0, q0, q_1]
		bd1 = Bond("BD_IN", qnums)

		qnums = [q1, q0, q0, q_1]
		bd2 = Bond("BD_IN", qnums)

		bd2.combine(bd1)
		degs = bd2.degeneracy()
		self.assertEqual("BD_IN", bd2.type())
		self.assertEqual(24, bd2.dim())
		self.assertEqual(1, degs[q_2])
		self.assertEqual(5, degs[q_1])
		self.assertEqual(9, degs[q0])
		self.assertEqual(7, degs[q1])
		self.assertEqual(2, degs[q2])


if __name__ == "__main__":
	unittest.main()